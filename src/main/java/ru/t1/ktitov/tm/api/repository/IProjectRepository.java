package ru.t1.ktitov.tm.api.repository;

import ru.t1.ktitov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}
