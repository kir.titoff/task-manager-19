package ru.t1.ktitov.tm.command;

import ru.t1.ktitov.tm.api.model.ICommand;
import ru.t1.ktitov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public IServiceLocator getServiceLocator() {
        return this.serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result = result + name;
        if (argument != null && !argument.isEmpty()) result = result + ", " + argument;
        if (description != null && !description.isEmpty()) result = result + " - " + description;
        return result;
    }

}
